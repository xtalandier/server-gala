var auth  = require('../modules/authentication')
  , fs    = require('fs')
  , utils = require('../modules/utils')
  , File  = require('../models/').File;

var Authenticated = module.exports = function(app){
  app.get('/admin' , function(req , res){
    checkAuth(req , res , function(){
      res.send('admin');
    });
  });
  app.get('/admin/moderate' , function(req , res){
      
      res.header("Access-Control-Allow-Origin", req.headers.origin || "*");
		res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE"); 
		res.header("Access-Control-Allow-Headers", "X-Requested-With");
      
    checkAuth(req , res , function(){
      File.find({
        where : {validated : 0},
        order : 'RAND()'
      }).success(function(f){
        res.send({
          error : f === null ? 'no-file' : '',
          guid  : f === null ? ''        : f.guid ,
          url   : f === null ? ''        : '/upload/' + f.guid + '.jpg'
        });
      });
    });
  });
  app.post('/admin/moderate' , function(req , res){
      res.header("Access-Control-Allow-Origin", req.headers.origin || "*");
      res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE"); 
      res.header("Access-Control-Allow-Headers", "X-Requested-With");
      
    var data = eval('(' + req.body.data + ')');
    var guid   = data.guid;
    var accept = data.accept; // 1: Oui, 2: Non 
    checkAuth(req , res , function(){
      File.find({
        where : {validated : 0 , guid : guid}
      }).success(function(f){
        if(f === null){
          res.send('no-file');
        }else{
          f.validated = accept;
          f.save().success(function(){
            res.send('ok');
          });
        }
      });
    });
  });
}



var checkAuth = function(req , res , callback){
  auth.autoLogin(req.param('token') , function(user){
    if(user){
      callback(user);
    }else{
      res.send('bad-token');
    }
  });
};



