var auth  = require('../modules/authentication')
  , fs    = require('fs')
  , utils = require('../modules/utils')
  , File  = require('../models/').File;

var Authenticated = module.exports = function(app){
  app.get('/authenticated' , function(req , res){
    checkAuth(req , res , function(){
      res.send('authenticated');
    });
  });

  app.post('/authenticated/publish' , function(req , res){
    checkAuth(req , res , function(user){
		res.header("Access-Control-Allow-Origin", req.headers.origin || "*");
		res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE"); 
		res.header("Access-Control-Allow-Headers", "X-Requested-With");
	
      if(req.files.file.type != 'image/jpeg'){
        res.send('error-only-jpeg');
        return false;
      }
      fs.readFile(req.files.file.path, function (err, data) {
	  
	  console.log(req.body);
	  
        var head = JSON.parse(req.body.pack).head ;
        var fName = head.guid;
        var newPath = __dirname + '/../public/upload/' + fName + '.jpg';
        fs.writeFile(newPath, data, function (err) {
          if(err){
            res.send(err);
          }
          if(fs.existsSync(newPath)){
            File.create({
              guid    : fName ,
              comment : 'coucou' , //req.param('txtcomment'),
              UserId  : user.id  ,
              validated : 0
            }).success(function(){
              res.send('file-uploaded');
            });
          }else{
            res.send('error-something-went-wrong');
        }
        });
      });
    });
  });
};

var checkAuth = function(req , res , callback){
  auth.autoLogin(req.param('token') , function(user){
    if(user){
      callback(user);
    }else{
      res.send('bad-token');
    }
  });
};

