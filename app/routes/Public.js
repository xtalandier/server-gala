var Model   = require('../models')
  , User    = Model.User
  , File    = Model.File
  , Isadmin = Model.Isadmin
  , crypto  = require('crypto')
  , auth    = require('../modules/authentication')
  , fs    = require('fs');

var Public = module.exports = function(app){
  app.get('/public' , function(req , res){
    res.send('index');
  });

  app.get('/photos' , function(req , res){
    var path = require('path') , fs = require('fs');
    var p = "./app/public/upload";
    str = "";
    fs.readdir(p, function (err, files) {
      if (err) {
          throw err;
      }
      files.map(function (file) {
          return file;
      }).filter(function (file) {
        return true;
      }).forEach(function (file) {
        str+= '<a style="margin:20px;" href="/upload/' + file + '" target="_blank"><img src="/upload/' + file + '" style="width:100px;border:4px black solid;" /></a>';        
      });
      res.send(str);
    });
  });

  app.get('/public/login' , function(req , res){
    res.render('public/login');
  });

  app.post('/public/login' , function(req , res){
//  console.log(req);
  res.header("Access-Control-Allow-Origin", req.headers.origin || "*");
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE"); 
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  
	var data  = req.body.data;//= eval('(' + req.body.data + ')');
    var uid    = data.uid;
    var passwd = data.password;
    auth.manualLogin(uid , passwd , function(error , user){
      if(error === null){
        Isadmin.find({
          where : {id: user.id}
        }).success(function(adm){
          res.send(JSON.stringify({is_admin:adm.is_admin , data : user.token}));
        });
      }else{
        res.send(JSON.stringify({is_admin:0 , data : error}));
      }
    });
  });


  app.get('/public/wall' , function(req , res){
    res.render('public/wall');
  });

  app.get('/public/last-photos' , function(req , res){
    File.find({
      where : {validated : 1},
      order : 'RAND()'
    }).success(function(f){
      res.send(f === null ? 'no-file' : '/upload/' + f.guid + '.jpg');
    });
  });
};

var md5 = function(str) {
  return crypto.createHash('md5').update(str).digest('hex');
}
