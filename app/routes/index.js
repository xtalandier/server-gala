var router = module.exports = function(app){
  var routes = [
    'Public',
    'Authenticated',
    'Admin'
  ];
  routes.forEach(function(route) {
    module.exports[route] = require('./' + route)(app);
  });
}

