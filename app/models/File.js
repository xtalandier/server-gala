module.exports = function(sequelize, DataTypes) {
  return sequelize.define('File', {
    id        : DataTypes.INTEGER ,
    guid      : DataTypes.STRING  ,
    comment   : DataTypes.STRING  ,
    UserId    : DataTypes.STRING  ,
    validated : DataTypes.INTEGER
  }, {
    instanceMethods: {
    }
  });
};
