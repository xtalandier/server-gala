module.exports = function(sequelize, DataTypes) {
  return sequelize.define('User', {
    nickname      : DataTypes.STRING ,
    passwd   : DataTypes.STRING  ,
    token    : DataTypes.STRING
  }, {
    instanceMethods: {
    }
  });
};
