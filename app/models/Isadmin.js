module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Isadmin', {
    id        : DataTypes.INTEGER ,
    is_admin  : DataTypes.INTEGER
  }, {
    instanceMethods: {
    }
  });
};
