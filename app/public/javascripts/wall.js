var lastID = 0;
var photos  = [];
var pub = [];
var curPub = 0;

$(document).ready(function(){
  getImage();
  $.get('/pub/pub.txt' , function(data){
    pub = eval('(' + data + ')');
  });
});


var sens    = -1;
var nbAdded = 0;
var timer   = 1000;
var nbBetweenPubs = 4;
function getImage(){
  $.get('/public/last-photos' , {last : lastID} , function(url){
    if(++nbAdded % 10 == 0) $('.photo-container').remove();
    if(nbAdded % nbBetweenPubs == 0){
      showPub();
    }else{
      $('#pub-container').html('').hide();
      var $photo = $('<div class="photo-container"></div>');
      $('<img class="photo" src="' + url + '" />').appendTo($photo);
      $photo.appendTo($('body'));
      centerImage($photo);
      timer = 1000;
    }
    setTimeout(getImage, timer);
  });
}
function centerImage($photo){
  var width  = $(window).width();
  var height = $(window).height();
  var cx = width  / 2 - $photo.width()  / 2;
  var cy = height / 2 - $photo.height() / 2 - 100;
  $photo.css('left' , cx + 'px').css('top' , cy);
  $photo.css('transform' , 'scale(2)').css('display' , '');
  sens *= -1;
  $photo.transition({
    scale  : 1 ,
    rotate : sens * (Math.floor(Math.random() * 45) + 1) + 'deg' 
  });
}

function showPub() {
  $('#pub-container').show();
  var laPub = pub[curPub];
  var data  = '';
  switch(laPub.type){
    case 'image':
      data = '<img src="%url%" />';
      break;
    case 'video':
      data = '<video src="%url%" autoplay></video>';
      break;
    default:
      data = '%url%';
      break;
  }
  timer = laPub.duration * 1000;
  $('#pub-container').html(data.replace('%url%' , '/pub/' + laPub.file));
  curPub++

  if(curPub >= pub.length) curPub = 0;
}
