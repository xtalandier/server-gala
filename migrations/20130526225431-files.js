module.exports = {
    up: function (migration, DataTypes , done) {
        migration.createTable(
            'Files', {
            id  : {
              type : DataTypes.INTEGER   ,
              autoIncrement : true ,
              primaryKey    : true
            } ,
            uid     : DataTypes.STRING  ,
            comment : DataTypes.STRING  ,
            UserId  : DataTypes.INTEGER ,
            createdAt : DataTypes.DATE  ,
            updatedAt : DataTypes.DATE
        }, {
            engine: 'InnoDB', // default: 'InnoDB'
            charset: 'latin1' // default: null
        });
        done();
    },
    down: function (migration, DataTypes, done) {
      migration.dropTable('Files');
      done();
    }
}