var helper = require('./testHelper')
  , User = require('../../app/models/').User;

var user = null;

describe("Publish", function(){

  before(function(done){
    User.findOrCreate({
      nickname : 'publish-user' ,
      email    : 'publish@example.com' ,
      passwd   : 'mypassword' ,
      token    : 'a-simple-token-for-publish'
    }).success(function(user){
      done();
    });
  });

  describe("#publish a file", function(){
    it("should upload an image", function(done){
      helper.visit('/authenticated/publish/?token=a-simple-token-for-publish', function(err, browser, status){
        if(err) done(err);
        browser
          .fill('txtcomment' , 'This is a comment')
          .attach("txtfile"  , __dirname + '/../files/xavier.jpg')
          .pressButton('Upload' , function(){
            browser.body.innerHTML.should.equal('file-uploaded');
            done();
          });
      });
    });

    it("should not upload a file wich is not an image", function(done){
      helper.visit('/authenticated/publish?token=a-simple-token-for-publish', function(err, browser, status){
        if(err) done(err);
        browser
          .fill('txtcomment' , 'This is a comment')
          .attach("txtfile"  , __dirname + '/../files/textfile.txt')
          .pressButton('Upload' , function(){
            browser.body.innerHTML.should.equal('error-only-jpeg');
            done();
          });
      });
    });
  });
});
